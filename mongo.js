

use sena344


db.createCollection("usuarios")


db.usuarios.insertMany([{
	
	nombre: 'Juan',	
	apellido: 'Perez',
	telefono: '3132345678',
	documento: '3456789',
	direccion: 'Calle 65 No. 25-96'

}, 
{

	nombre: 'Maria',	
	apellido: 'Perez',
	telefono: '36985656',
	documento: '1256698',
	direccion: 'Carrera 4 No 56-98'

},
{

	nombre: 'Tereza',	
	apellido: 'Quintero',
	telefono: '310456787',
	documento: '8976543',
	direccion: 'Carrera 12 Sur No 56-98'

}
])

// db.usuarios.find()
// db.usuarios.find().pretty()
//db.usuarios.find({"documento":"1256698"}).pretty()
//db.usuarios.remove({"documento":"1256698"})

db.createCollection("autenticacion")


db.autenticacion.insertMany([{
	
	usuario: new ObjectId("5fc03b376b1226dadfa86ef3"),
	correo: 'juanp@gmail.com',
	pas: '123'

}, 
{

	usuario: new ObjectId("5fc03b376b1226dadfa86ef4"),
	correo: 'mariap@gmail.com',
	pas: '345'

},
{

	usuario: new ObjectId("5fc03b376b1226dadfa86ef5"),
	correo: 'terezaq@gmail.com',
	pas: '987'

}
])



db.usuarios.aggregate(
	[
		{
			$match:{
				_id : ObjectId("5fc03b376b1226dadfa86ef5"),
			}
		},
		{
			$lookup:{
				from: 'autenticacion',
				localField: '_id',
				foreignField: 'usuario',
				as: 'datosLogin'
			}
		},
		{
			$unwind: '$datosLogin'
		},
		{
			$project:{
				nombreUsuario: '$nombre',
				apellidoUsuario: '$apellido',
				correoUsuario: '$datosLogin.correo',
				passwordUsuario: '$datosLogin.pas'
			}
		}
	]
	).pretty()