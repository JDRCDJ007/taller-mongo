

use institute


//Creación de Colecciones
db.createCollection('teachers')
db.createCollection('students')
db.createCollection('courses')
db.createCollection('courseTeachers')
db.createCollection('courseStudents')

//Insercciones para la tabla de profesores
db.teachers.insertMany([{

	name: 'Aiden David',
	lastName: 'Rodríguez Scott',
	email: 'aidenRo@gmail.com',
	document: '1000519728',

},
{

	name: 'Arnold Stuart',
	lastName: 'Philips Fork',
	email: 'ArnoldS123@gmail.com',
	document: '1000596874',

},
{

	name: 'Teresa María',
	lastName: 'Muñoz Torres',
	email: 'teremuño@gmail.com',
	document: '1000359854',

},
{

	name: 'Frederic Stiven',
	lastName: 'Ocasio Fly',
	email: 'FredericOcaaa@gmail.com',
	document: '',

},
{

	name: 'Henry Enrique',
	lastName: 'Alomar Piers',
	email: 'alomaHenry@gmail.com',
	document: '1852695425',

}
])
db.teachers.find().pretty()

//Insercciones para la tabla de estudiantes
db.students.insertMany([{

	name: 'Nicolae',
	lastName: 'Cantero Pérez',
	email: 'nicola@gmail.com',
	document: '1000285569',

},
{

	name: 'Consuelo Andrea',
	lastName: 'Cabeza Onion',
	email: 'consu45@gmail.com',
	document: '1235698545',

},
{

	name: 'Domingo Alexander',
	lastName: 'Castro Diaz',
	email: 'dom@gmail.com',
	document: '1000758496',

},
{

	name: 'Josue Hernando',
	lastName: 'Melendés Buitrago',
	email: 'josu@gmail.com',
	document: '1987456321',

},
{

	name: 'Jan',
	lastName: 'Quevedo Espitia',
	email: 'jan361@gmail.com',
	document: '1502586412',

},
{

	name: 'Barbara',
	lastName: 'Monge Lee',
	email: 'barbarata@gmail.com',
	document: '1205658745',

},
{

	name: 'María Felisa',
	lastName: 'Agudo Frank',
	email: 'maryfrank@gmail.com',
	document: '123654897',

},
{

	name: 'Georgina',
	lastName: 'Galvez Rodríguez',
	email: 'geor852@gmail.com',
	document: '1598745632',

},
{

	name: 'Natividad',
	lastName: 'Riquelme Fenix',
	email: 'natiri@gmail.com',
	document: '95685685745',

},
{

	name: 'Carlota Felix',
	lastName: 'Bonet Velandia',
	email: 'carlo@gmail.com',
	document: '1003525845',

},
{

	name: 'Catalina',
	lastName: 'Valdes Veléz',
	email: 'cata@gmail.com',
	document: '1202526354',

},
{

	name: 'Ines',
	lastName: 'Amador Duarte',
	email: 'inea@gmail.com',
	document: '1256856987',

},
{

	name: 'Lola Mercedez',
	lastName: 'Carranza Pajón',
	email: 'lolita07@gmail.com',
	document: '1000369852',

},
{

	name: 'Carmen del Pilar',
	lastName: 'Alfonso Pazos',
	email: 'carmelita456@gmail.com',
	document: '1000897456',

},
{

	name: 'Maria Esther',
	lastName: 'Perdomo Pumarejo',
	email: 'esthermary@gmail.com',
	document: '1000568987',

},
{

	name: 'Cristina',
	lastName: 'Montero Palacios',
	email: 'cristi@gmail.com',
	document: '1000758495',

},
{

	name: 'Maria Teresa',
	lastName: 'Fuertes Cifuentes',
	email: 'Tere52@gmail.com',
	document: '1000758485',

},
{

	name: 'Manuel Alejandro',
	lastName: 'Ramirez Ropero',
	email: 'manuetre@gmail.com',
	document: '1748598654',

},
{

	name: 'Federico',
	lastName: 'Saez Smith',
	email: 'saezS12@gmail.com',
	document: '1000325645',

},
{

	name: 'Ibon',
	lastName: 'Bosch King',
	email: 'ibon0098@gmail.com',
	document: '1003568987',

}

])
db.students.find().pretty()

//Insercciones para la tabla de profesores
db.courses.insertMany([{

	nameCredits: 'Cálculo Diferencial',
	credits: '20'

},
{

	nameCredits: 'Drámatica Contemporánea'
	credits: '15'

},
{

	nameCredits: 'Literatura Inglesa'
	credits: '15'

},
{

	nameCredits: 'Programación Avanzada JS'
	credits: '18'

},
{

	nameCredits: 'Analisis de Bases de Datos'
	credits: '20'

},
{

	nameCredits: 'Música Clásica - Origenes'
	credits: '20'

},
{

	nameCredits: 'Comunicación Social'
	credits: '30'

},
{

	nameCredits: 'Arte Escenico'
	credits: '18'

}
])
db.courses.find().pretty()

//Insercciones para la tabla de Cursos Profesores
db.courseTeachers.insertMany([{

	userTeacher: new ObjectId('5fcfe1931974a765970ae196'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')}	
	]

},
{

	userTeacher: new ObjectId('5fcfe1931974a765970ae197'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b0')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b1')},
	]

},
{

	userTeacher: new ObjectId('5fcfe1931974a765970ae198'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b3')},
	]

},
{

	userTeacher: new ObjectId('5fcfe1931974a765970ae199'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b5')},
	]

},
{

	userTeacher: new ObjectId('5fcfe1931974a765970ae19a'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

}

])


//Insercciones para la tabla de Cursos Estudiantes
db.courseStudents.insertMany([{

	userStudent: new ObjectId('5fcfe1da1974a765970ae19b'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b1')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b3')}
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae19c'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b3')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b5')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')}
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae19d'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')}
	]

},{

	userStudents: new ObjectId('5fcfe1da1974a765970ae19e'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')}	
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae19f'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b0')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a0'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b3')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a1'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b5')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a2'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a3'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

},
{

	userStudents: new ObjectId('5fcfe1da1974a765970ae1a4'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')}	
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a5'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b0')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b1')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a6'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a7'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b5')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a8'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1a9'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

},{

	userStudents: new ObjectId('5fcfe1da1974a765970ae1aa'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1af')}	
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1ab'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b0')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b1')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1ac'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b2')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b3')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1ad'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b4')},
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b5')},
	]

},
{

	userStudent: new ObjectId('5fcfe1da1974a765970ae1ae'),
	course: [
		{idCourse: new ObjectId ('5fcfe2091974a765970ae1b6')}
	]

}
])


db.teachers.aggregate(
	[
		{
			$match:{
				_id : ObjectId("5fcfe1931974a765970ae198"),
			}
		},
		{
			$lookup:{
				from: 'courseTeachers',
				localField: '_id',
				foreignField: 'userTeacher',
				as: 'datesCourse'
			}
		},
		{
			$unwind: '$datesCourse'
		},
		{
			$project:{
				nameTeacher: '$name',
				lastNameTeacher: '$lastName',
				courses: '$datesCourse.course',
			}
		}

	]
	).pretty()




db.students.aggregate(
	[
		{
			$match:{
				_id : ObjectId("5fcfe1da1974a765970ae19c"),
			}
		},
		{
			$lookup:{
				from: 'courseStudents',
				localField: '_id',
				foreignField: 'userStudent',
				as: 'datesCourse'
			}
		},
		{
			$unwind: '$datesCourse'
		},
		{
			$project:{
				nameStudent: '$name',
				lastNameStudent: '$lastName',
				courses: '$datesCourse.course',
			}
		}

	]
	).pretty()



//Alternativa para realizar o simular un INNER JOIN

use institute2

db.createCollection('teachers')
db.createCollection('students')
db.createCollection('courses')


//Insercciones para la tabla de profesores
db.teachers.insertMany([{

	name: 'Aiden David',
	lastName: 'Rodríguez Scott',
	email: 'aidenRo@gmail.com',
	document: '1000519728',

},
{

	name: 'Arnold Stuart',
	lastName: 'Philips Fork',
	email: 'ArnoldS123@gmail.com',
	document: '1000596874',

},
{

	name: 'Teresa María',
	lastName: 'Muñoz Torres',
	email: 'teremuño@gmail.com',
	document: '1000359854',

},
{

	name: 'Frederic Stiven',
	lastName: 'Ocasio Fly',
	email: 'FredericOcaaa@gmail.com',
	document: '',

},
{

	name: 'Henry Enrique',
	lastName: 'Alomar Piers',
	email: 'alomaHenry@gmail.com',
	document: '1852695425',

}
])
db.teachers.find().pretty()


//Insercciones para la tabla de profesores
db.courses.insertMany([{

	nameCredits: 'Cálculo Diferencial',
	credits: '20',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9fead')

},
{

	nameCredits: 'Drámatica Contemporánea',
	credits: '15',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feae')

},
{

	nameCredits: 'Literatura Inglesa',
	credits: '15',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feaf')

},
{

	nameCredits: 'Programación Avanzada JS',
	credits: '18',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feb0')

},
{

	nameCredits: 'Analisis de Bases de Datos',
	credits: '20',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feb0')

},
{

	nameCredits: 'Música Clásica - Origenes',
	credits: '20',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9fead')

},
{

	nameCredits: 'Comunicación Social',
	credits: '30',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feb1')

},
{

	nameCredits: 'Arte Escenico',
	credits: '18',
	teacher: new ObjectId('5fd15ad30b30ee8e2bd9feb1')

}
])
db.courses.find().pretty()


//Insercciones para la tabla de estudiantes
db.students.insertMany([{

	name: 'Nicolae',
	lastName: 'Cantero Pérez',
	email: 'nicola@gmail.com',
	document: '1000285569',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb2')

},
{

	name: 'Consuelo Andrea',
	lastName: 'Cabeza Onion',
	email: 'consu45@gmail.com',
	document: '1235698545',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb2')

},
{

	name: 'Domingo Alexander',
	lastName: 'Castro Diaz',
	email: 'dom@gmail.com',
	document: '1000758496',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb2')

},
{

	name: 'Josue Hernando',
	lastName: 'Melendés Buitrago',
	email: 'josu@gmail.com',
	document: '1987456321',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb3')

},
{

	name: 'Jan',
	lastName: 'Quevedo Espitia',
	email: 'jan361@gmail.com',
	document: '1502586412',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb3')

},
{

	name: 'Barbara',
	lastName: 'Monge Lee',
	email: 'barbarata@gmail.com',
	document: '1205658745',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb4')

},
{

	name: 'María Felisa',
	lastName: 'Agudo Frank',
	email: 'maryfrank@gmail.com',
	document: '123654897',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb4')

},
{

	name: 'Georgina',
	lastName: 'Galvez Rodríguez',
	email: 'geor852@gmail.com',
	document: '1598745632',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb5')

},
{

	name: 'Natividad',
	lastName: 'Riquelme Fenix',
	email: 'natiri@gmail.com',
	document: '95685685745',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb5')

},
{

	name: 'Carlota Felix',
	lastName: 'Bonet Velandia',
	email: 'carlo@gmail.com',
	document: '1003525845',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb6')

},
{

	name: 'Catalina',
	lastName: 'Valdes Veléz',
	email: 'cata@gmail.com',
	document: '1202526354',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb6')

},
{

	name: 'Ines',
	lastName: 'Amador Duarte',
	email: 'inea@gmail.com',
	document: '1256856987',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb7')

},
{

	name: 'Lola Mercedez',
	lastName: 'Carranza Pajón',
	email: 'lolita07@gmail.com',
	document: '1000369852',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb7')

},
{

	name: 'Carmen del Pilar',
	lastName: 'Alfonso Pazos',
	email: 'carmelita456@gmail.com',
	document: '1000897456',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb8')

},
{

	name: 'Maria Esther',
	lastName: 'Perdomo Pumarejo',
	email: 'esthermary@gmail.com',
	document: '1000568987',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb8')

},
{

	name: 'Cristina',
	lastName: 'Montero Palacios',
	email: 'cristi@gmail.com',
	document: '1000758495',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb9')

},
{

	name: 'Maria Teresa',
	lastName: 'Fuertes Cifuentes',
	email: 'Tere52@gmail.com',
	document: '1000758485',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb9')

},
{

	name: 'Manuel Alejandro',
	lastName: 'Ramirez Ropero',
	email: 'manuetre@gmail.com',
	document: '1748598654',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb2')

},
{

	name: 'Federico',
	lastName: 'Saez Smith',
	email: 'saezS12@gmail.com',
	document: '1000325645',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb2')

},
{

	name: 'Ibon',
	lastName: 'Bosch King',
	email: 'ibon0098@gmail.com',
	document: '1003568987',
	courseBelonging: new ObjectId('5fd15b840b30ee8e2bd9feb3')

}

])
db.students.find().pretty()


db.teachers.aggregate(
	[
		{

			$match: {
				_id: ObjectId('5fd15ad30b30ee8e2bd9fead')
			}

		},
		{

			$lookup: {
				from: 'courses',
				localField: '_id',
				foreignField: 'teacher',
				as: 'courses'
			}

		},
		{

			$unwind: '$courses'

		}, 
		{
			$lookup: {

				from: 'students',
				localField: 'courses._id',
				foreignField: 'courseBelonging',
				as: 'students'

			}
		}
	]
).pretty()